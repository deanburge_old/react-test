# React Test

- users can see a list of article teasers
- users can press a button to load more articles

Details

- use React for rendering the HTML list and button ui
- use the Flux data flow pattern
- code must be unit tested
- use local JSON files as pages of articles to display
- all articles have unique ids, titles and images. one article is:

```
{
	id: 1,
	title: "article title 1",
	image: "http://placehold.it/300x250&text=image 1"
}
```
